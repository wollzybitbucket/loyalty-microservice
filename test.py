from main import app

import json
from unittest import TestCase, main, TextTestRunner


jwt_header = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjYXNoYm94X2lkIjoiNjc4NDY1MyIsInBhcnRuZXJfaWQiOiI3MzQ4NjczNDgiLCJ1c2VyX2lkIjoiMzI0MzI1In0.kNyJm8gXyNBPxUCYh_sA4THF9-ok9YV7kja1I0YGKs8'


class Tests(TestCase):
    
    def setUp(self):
        self.cli = app.test_client()

    def test_loyalty(self):
        response = self.cli.get('/loyalty/property', query_string={'serial_number_id':'1231231'}, headers = {'Authorization': jwt_header})
        data = response.get_json()
        expected = {
            "status": "Ok",
            "data": {
                "discount": 3.3,
                "percentage_cashback": 90,
                "balance_cashback": 550
            }
        }
        assert data == expected

        response = self.cli.get('/loyalty/property', headers = {'Authorization': jwt_header})
        data = response.get_json()
        expected = {
            'error': {
                'code': 400,
                'message': "serial_number_id is required."
            },
            'status': "Error",
            'data': ""
        }
        assert data == expected

    def test_check_loayalty(self):
        request_data = {
            "serial_number_id": 0,
            "total": 0,
            "used_balance": 0
        }
        response = self.cli.patch('/loyalty/check/property', data = json.dumps(request_data), content_type = 'application/json',headers = {'Authorization': jwt_header})
        data = response.json()
        expected = {
            "status": "Ok",
            "data": {
                "discount": 3.3,
                "percentage_cashback": 90,
                "balance_cashback": 550
            }
        }
        assert expected == data

if __name__ == "__main__":
    log_file = 'test_log.txt'
    f = open(log_file, "w")
    runner = TextTestRunner(f)
    main(testRunner=runner)
    f.close()

