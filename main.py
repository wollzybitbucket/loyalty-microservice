from flask import Flask, request, jsonify


app = Flask(__name__)


def make_error(code, message):
    err = {
        'error': {
            'code': code,
            'message': message
        },
        'status': "Error",
        'data': ""
    }
    return jsonify(err)


@app.route('/loyalty/property', methods=['GET'])
def property():
    if 'serial_number_id' in request.args:

        # get from db

        return jsonify({
            "status": "Ok",
            "data": {
                "discount": 3.3,
                "percentage_cashback": 90,
                "balance_cashback": 550
            }
        })
    else:
        return make_error(400, "serial_number_id is required.")


@app.route('/loyalty/check/property', methods=['PATCH'])
def check_property():
    data = request.json
    
    # alter db

    return jsonify({
        "status": "Ok",
        "data": {
            "discount": 3.3,
            "percentage_cashback": 90,
            "balance_cashback": 550
        }
    })


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)